<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Category;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Product::create([
			"name"=> "Sik Sik Wat",
			"category"=> $this->categoryExtract("Ethiopia, Meat, Beef, Chili pepper"),
			"sku"=> "DISH999ABCD",
			"price"=> "13.49"
		]);
		Product::create([
			"name"=> "Huo Guo",
			"category"=> $this->categoryExtract("China, Meat, Beef, Fish, Tofu, Sichuan pepper"),
			"sku"=> "DISH234ZFDR",
			"price"=> "11.99"
		]);
		Product::create([
			"name"=> "Cau-Cau",
			"category"=> $this->categoryExtract("Peru, Potato, Yellow Chili pepper"),
			"sku"=> "DISH775TGHY",
			"price"=> "15.29"
		]);
    }

    public function categoryExtract($string)
    {
    	$cat = explode(',', $string);
    	$catArray = [];
    	foreach ($cat as $c) {
    		if(!ctype_space($c) || !empty($c))
    		{
    			$category = Category::firstOrNew(['name' =>  $c]);
    			$category->name = $c;
    			$category->save();
    			$catArray[] = $category->id;
    		}
    	}
    	$out = array_values($catArray);
		return json_encode($out);
    }
}
