<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;

class ProductTest extends TestCase
{

    public function testProductShow()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        $product = Product::create([
            "name" => "Product Name",
            "category" => "[2,4,6]",
            "sku" => "SKU",
            "price" => "33.99"
        ]);

        $this->json('GET', 'api/product/' . $product->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "product" => [
                    "name" => "Product Name",
                    "category" => "[2,4,6]",
                    "sku" => "SKU",
                    "price" => "33.99"
                ],
                "message" => "Retrieved successfully"
            ]);
    }

    public function testProductUpdate()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        $product = Product::create([
            "name" => "Product Name",
            "category" => "[2,4,6]",
            "sku" => "SKU",
            "price" => "33.99"
        ]);

        $payload = [
            "name" => "New Product Name",
            "category" => "[1,3,5]",
            "sku" => "New SKU",
            "price" => "12.99"
        ];
        //CAN ALSO USE PATCH
        $this->json('PUT', 'api/product/' . $product->id , $payload, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "product" => [
                    "name" => "New Product Name",
                    "category" => "[1,3,5]",
                    "sku" => "New SKU",
                    "price" => "12.99"
                ],
                "message" => "Updated successfully"
            ]);
    }

    public function testProductDelete()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        $product = Product::create([
            "name" => "New Product Name",
            "category" => "[1,3,6]",
            "sku" => "New SKU",
            "price" => "33.99"
        ]);

        $this->json("DELETE", '/api/product/'.$product->id,  ['Accept' => 'application/json'])
            ->assertStatus(204);
    }
}
