Restfull API for SpiceDeli 

SETUP 
1. Git clone the application 
2. run composer update
3. Open .env file and update the Database settings to point to your mysql connection.
4. Create a database in the mysql database
5. run php artisan migrate
6. run php artisan passport:install
7. run php artisan db:seed --ProductTableSeeder
8. run php artisan serve

API testing 

I have done all testing on POSTMAN 

1. Set Application header - Accept - application/json
2. run the register api as follows 
	a. POST http://localhost:8000/api/register
		enter the param as follows 
		name = Kablamus Developer
		email = kablamus@developer.com
		password = 123456
		c_password = 123456
	This will register the new user in to the system 
3. run the login api as follows 
	a. POST http://localhost:8000/api/login 
		enter the param as follows 
		email = kablamus@developer.com
		password = 123456
		This will login the user and create an acceptance token
		
		COPY THIS TOKEN 
		
4. run list all product api as follows 
	a. set the Authorization to Bearer Token -> paste the above copied token in the field. (Other wise you will get unathorized error)
	b. GET http://localhost:8000/api/product
	
5. run show one product api as follows 
	a. GET http://localhost:8000/api/product/2
	b. you can substitute "2" with your product id

6. run list all Category api as follows 
	a. GET http://localhost:8000/api/category
	
7. run store product api as follows 
	a. POST http://localhost:8000/api/product
	b. enter the following params 
		name = "name of the product"
		category = "This is a json field and accepts only ids for example: [1,2,3]"
		sku = "SKU"
		price = "This is a float field for example: 12.35"

8. run update product api as follows 
	a. PUT http://localhost:8000/api/product/2
	b. you can substitute "2" with your product id which you wish to update.
	c. you can update one field or replace all the fields in product. 
	d. enter the following params 
		name = "New name for the product"
		category = "New category for example: [1,2,3]"
		sku = "NEW SKU"
		price = "New Price"
		
9. run delete product api as follows 
	a. DELETE http://localhost:8000/api/product/2
	b. you can substitute "2" with your product id which you wish to delete.
		
PHPUnit TEST 

Files are in test/Feature/ folder 

for the purpose of this example i am writing Tests for the following features 
1. Registration - check for validation and successfull registration
2. Login - check for validation and successfull login
3. Product - list, store, update, delete