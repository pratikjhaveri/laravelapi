<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Category;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category' => $this->getCategory($this->category),
            'sku' => $this->sku,
            'price' => $this->price,
            'created_at' => $this->created_at->format('m/d/Y, h:i'),
            'updated_at' => $this->updated_at->format('m/d/Y, h:i'),
        ];
    }

    private function getCategory($json)
    {
        $category = json_decode($json);
        $catName = [];
        foreach ($category as $cat) {
            $name = Category::find($cat);
            $catName[] = $name->name;
        }

        $out = array_values($catName);
        return json_encode($out);
    }
}
