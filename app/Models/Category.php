<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Category extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    protected $fillable = ['name'];

    protected $casts = [
        'category' => 'json',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Products', 'category');
    }
}
