<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'category','sku','price'];

    public function category()
    {
        return $this->hasMany('App\Models\Category', 'id');
    }
}
